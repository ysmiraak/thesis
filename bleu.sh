#!/usr/bin/env bash

# pip install --user sacrebleu

sacrebleu -tok intl -b -i lab/pred/$1 lab/data/valid_tgt.txt

from src.util.fn import Record, identity, partial
from src.util.nn import Normalize, Smooth, Dropout, Embed, Conv, Attention
from src.util.np import np, partition
from src.util.tf import tf, f32, i32, scope, placeholder, trim, size, seq2bow, bow2seq, poisson


def causal_mask(t, name= 'causal_mask'):
    """returns the causal mask for `t` steps"""
    with scope(name):
        return tf.linalg.LinearOperatorLowerTriangular(tf.ones((t, t))).to_dense()


def sinusoid(dim, time, freq= 1e-4, array= False):
    """returns a rank-2 tensor of shape `dim, time`, where each column
    corresponds to a time step and each row a sinusoid, with
    frequencies in a geometric progression from 1 to `freq`.

    """
    assert not dim % 2
    if array:
        a = (freq ** ((2 / dim) * np.arange(dim // 2))).reshape(-1, 1) @ (1 + np.arange(time).reshape(1, -1))
        return np.concatenate((np.sin(a), np.cos(a)), -1).reshape(dim, time)
    else:
        assert False # figure out a better way to do this
        a = tf.reshape(
            freq ** ((2 / dim) * tf.range(dim // 2, dtype= tf.float32))
            , (-1, 1)) @ tf.reshape(
                1 + tf.range(f32(time), dtype= tf.float32)
                , (1, -1))
        return tf.reshape(tf.concat((tf.sin(a), tf.cos(a)), axis= -1), (dim, time))


class Sinusoid(Record):

    def __init__(self, dim, cap= None, name= 'sinusoid'):
        self.dim = dim
        self.name = name
        with scope(name):
            self.pos = tf.constant(sinusoid(dim, cap, array= True), tf.float32) if cap else None

    def __call__(self, time, name= None):
        with scope(name or self.name):
            return sinusoid(self.dim, time) if self.pos is None else self.pos[:,:time]


class MlpBlock(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.lin  = Conv(4*dim, dim, name= 'lin')
            self.lex  = Conv(dim, 4*dim, name= 'lex')
            self.norm = Normalize(dim)

    def __call__(self, x, dropout, name= None):
        with scope(name or self.name):
            return self.norm(x + dropout(self.lex(tf.nn.relu(self.lin(x)))))


class AttBlock(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.att  = Attention(dim)
            self.norm = Normalize(dim)

    def __call__(self, x, v, m, dropout, name= None):
        with scope(name or self.name):
            return self.norm(x + dropout(self.att(x, v, m)))


class Encode(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.blocks = AttBlock(dim, 's1') \
                ,         MlpBlock(dim, 'm1') \
                ,         AttBlock(dim, 's2') \
                ,         MlpBlock(dim, 'm2') \
                ,         AttBlock(dim, 's3') \
                ,         MlpBlock(dim, 'm3') \
                # ,         AttBlock(dim, 's4') \
                # ,         MlpBlock(dim, 'm4') \
                # ,         AttBlock(dim, 's5') \
                # ,         MlpBlock(dim, 'm5') \
                # ,         AttBlock(dim, 's6') \
                # ,         MlpBlock(dim, 'm6')

    def __call__(self, x, m, dropout, name= None):
        with scope(name or self.name):
            for block in self.blocks:
                btype = block.name[0]
                if   'c' == btype: x = block(x, dropout)
                elif 's' == btype: x = block(x, x, m, dropout)
                elif 'm' == btype: x = block(x, dropout)
                else: raise TypeError('unknown encode block')
            return x


class Decode(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.blocks = AttBlock(dim, 's1') \
                ,         AttBlock(dim, 'a1') \
                ,         MlpBlock(dim, 'm1') \
                ,         AttBlock(dim, 's2') \
                ,         AttBlock(dim, 'a2') \
                ,         MlpBlock(dim, 'm2') \
                ,         AttBlock(dim, 's3') \
                ,         AttBlock(dim, 'a3') \
                ,         MlpBlock(dim, 'm3') \
                # ,         AttBlock(dim, 's4') \
                # ,         AttBlock(dim, 'a4') \
                # ,         MlpBlock(dim, 'm4') \
                # ,         AttBlock(dim, 's5') \
                # ,         AttBlock(dim, 'a5') \
                # ,         MlpBlock(dim, 'm5') \
                # ,         AttBlock(dim, 's6') \
                # ,         AttBlock(dim, 'a6') \
                # ,         MlpBlock(dim, 'm6')

    def __call__(self, x, m, w, n, dropout, name= None):
        with scope(name or self.name):
            for block in self.blocks:
                btype = block.name[0]
                if   'c' == btype: x = block(x, dropout)
                elif 'b' == btype: x = block(x, x, m, w, n, dropout)
                elif 's' == btype: x = block(x, x, m, dropout)
                elif 'a' == btype: x = block(x, w, n, dropout)
                elif 'm' == btype: x = block(x, dropout)
                else: raise TypeError('unknown decode block')
            return x


class Bridge(Record):

    def __init__(self, dim, head= 8, bos= 1, name= 'bridge'):
        assert not dim % head
        self.bos  = bos
        self.dim  = dim
        self.head = head
        self.name = name
        with scope(name):
            self.att_block = AttBlock(dim, 'att')
            self.key_layer = Conv(dim, name= 'key')
            self.qry_layer = Conv(dim, name= 'qry')
            self.bow_layer = Conv(1, head, name= 'bow')

    def __call__(self, logit, value, mask, dropout, name= None):
        # logit :  dn  target logit matrix, whose bos=1 is used as query
        # value : bds  source sequence annotation
        # mask  : b1s  source attention mask
        #      -> bn   target bag-of-word
        with scope(name or self.name):
            d,h,c = self.dim, self.head, self.dim // self.head
            b,_,s = size(value)
            _ , n = size(logit)
            # query source annotation, todo consider dropout here
            _dn = tf.expand_dims(logit, 0)
            _d_ = tf.expand_dims(_dn[:,:,self.bos], 2)
            bd1 = self.att_block(_d_, value, mask, dropout)
            # query for bow, todo try mlp here
            bhc1 = tf.reshape(self.qry_layer(bd1), (b,h,c,1))
            _hcn = tf.reshape(self.key_layer(_dn), (1,h,c,n))
            bh1n = tf.matmul(bhc1, _hcn, transpose_a= True)
            bhn  = tf.squeeze(bh1n, 2) * (c ** -0.5)
            bn   = tf.squeeze(self.bow_layer(bhn), 1)
            return bn


class Model(Record):
    """-> Record

    model = Model.new( ... )
    train = model.data( ... ).train( ... )
    valid = model.data( ... ).valid( ... )

    """
    _new = 'dim_emb', 'dim_mid', 'dim_src', 'dim_tgt', 'cap', 'eos', 'bos'

    @staticmethod
    def new(dim_emb, dim_mid, dim_src, dim_tgt, cap, eos, bos):
        """-> Model with fields

          decode : Decode
          encode : Encode
         emb_tgt : Embed
         emb_src : Embed

        """
        assert not dim_emb % 2
        assert eos == 0
        return Model(
            step= tf.train.get_or_create_global_step()
            , bridge= Bridge(dim_emb, head= 8, bos= bos)
            , decode= Decode(dim_emb, name= 'decode')
            , encode= Encode(dim_emb, name= 'encode')
            , emb_tgt= Embed(dim_emb, dim_tgt, name= 'emb_tgt')
            , emb_src= Embed(dim_emb, dim_src, name= 'emb_src')
            , dim_emb= dim_emb
            , dim_tgt= dim_tgt
            , bos= bos
            , eos= eos
            , cap= cap + 1)

    def data(self, src= None, tgt= None):
        """-> Model with new fields

        position : Sinusoid
            src_ : i32 (b, ?)     source feed, in range `[0, dim_src)`
            tgt_ : i32 (b, ?)     target feed, in range `[0, dim_tgt)`
             src : i32 (b, s)     source with `eos` trimmed among the batch
             tgt : i32 (b, t)     target with `eos` trimmed among the batch
            mask : b8  (b, t)     target sequence mask
            true : i32 (?,)       target references
         max_tgt : i32 ()         maximum target length
         max_src : i32 ()         maximum source length
        mask_tgt : f32 (1, t, t)  target attention mask
        mask_src : f32 (b, 1, s)  source attention mask

        """
        src_ = placeholder(tf.int32, (None, None), src, 'src_')
        tgt_ = placeholder(tf.int32, (None, None), tgt, 'tgt_')
        with scope('src'):
            src, mask, len_src, max_src = trim(src_, eos= self.eos, cap= self.cap)
            mask_src = tf.log(tf.expand_dims(f32(mask), axis= 1))
        with scope('tgt'):
            tgt, mask, len_tgt, max_tgt = trim(tgt_, eos= self.eos, cap= self.cap)
            mask_tgt = tf.log(tf.expand_dims(f32(mask), axis= 1))
            true,tgt = tgt, tf.sort(tgt, axis= -1, direction= 'DESCENDING')
            bowt = seq2bow(tgt, self.dim_tgt, mask)
        return Model(
            position= Sinusoid(self.dim_emb, self.cap)
            , src_= src_, mask_src= mask_src, len_src= len_src, max_src= max_src, src= src
            , tgt_= tgt_, mask_tgt= mask_tgt, len_tgt= len_tgt, max_tgt= max_tgt, tgt= tgt
            , true= true, mask= mask, bowt= bowt
            , **self)

    def valid(self, dropout= identity, smooth= None):
        """-> Model with new fields, teacher forcing

           output : f32 (?, dim_tgt)  prediction on logit scale
             prob : f32 (?, dim_tgt)  prediction, soft
             pred : i32 (?,)          prediction, hard
        errt_samp : f32 (?,)          errors
        loss_samp : f32 (?,)          losses
             errt : f32 ()            error rate
             loss : f32 ()            mean loss

        """
        # encoding
        with scope('emb_src_'): w = self.position(self.max_src) + dropout(self.emb_src(self.src))
        w = self.encode(w, self.mask_src,                   dropout, name= 'encode_') # bds

        # bridging
        lmu = self.bridge(self.emb_tgt.logit, w, self.mask_src, dropout, name= 'bridge_')
        with scope('bowp_'):
            mu = tf.exp(lmu)
            bowp = tf.round(tf.stop_gradient(mu))

        with scope('bowl_'):
            bowt = f32(self.bowt)
            bowl_samp = tf.reduce_sum(poisson(bowt, mu, lmu= lmu), axis= -1)
            bowl = tf.reduce_mean(bowl_samp)

        with scope('errs_'):
            errs_samp = tf.reduce_sum(tf.abs(bowt - bowp), axis= -1)
            errs = tf.reduce_mean(errs_samp)

        return Model(
              bowp= bowp, lmu= lmu
            , bowl_samp= bowl_samp, bowl= bowl
            , errs_samp= errs_samp, errs= errs
            , **self)

    def train(self, dropout= 0.1, smooth= 0.1, warmup= 4e3, beta1= 0.9, beta2= 0.98, epsilon= 1e-9):
        """-> Model with new fields, teacher forcing

        step : i64 () global update step
          lr : f32 () learning rate for the current step
          up :        update operation

        along with all the fields from `valid`

        """
        dropout, smooth = Dropout(dropout, (None, self.dim_emb, None)), Smooth(smooth, self.dim_tgt)
        self = self.valid(dropout= dropout, smooth= smooth)
        with scope('down'):
            t = f32(self.step + 1)
            lr = (self.dim_emb ** -0.5) * tf.minimum(t ** -0.5, t * (warmup ** -1.5))
            down = tf.train.AdamOptimizer(lr, beta1, beta2, epsilon).minimize(self.bowl, self.step)
        return Model(self, dropout= dropout, smooth= smooth, lr= lr, down= down)

from src.util.tf import tf, f32, poisson, negative_binomial, zero_inflated_poisson, zero_inflated_negative_binomial
import matplotlib.pyplot as plt
import numpy as np

m,n = 6, 100
nat = np.arange(m)
lmu = np.linspace(-np.log(2*m), np.log(2*m), n)

nat = np.tile(np.expand_dims(nat, 1), (1, n)).astype(np.float32)
lmu = np.tile(np.expand_dims(lmu, 0), (m, 1)).astype(np.float32)

def plot(nll, plt= plt):
    if np.any(np.isnan(nll)): print("!!!! nan")
    for n in range(m): plt.plot(lmu[n], nll[n], label= f"{n}")
    plt.legend()

sess = tf.InteractiveSession()

nll = poisson(nat, lmu).eval()
plot(nll)
plt.show()

fig, axes = plt.subplots(ncols= 3, nrows= 3)
for axe, lth in zip(axes.ravel(), (-200, -20, -2, -1, 0, 1, 2, 20, 200)):
    nll = negative_binomial(nat, lmu, f32(lth)).eval()
    plot(nll, axe)
    axe.set_title(f"lth = {lth}")
plt.show()

fig, axes = plt.subplots(ncols= 3, nrows= 3)
for axe, lpi in zip(axes.ravel(), (-200, -20, -2, -1, 0, 1, 2, 20, 200)):
    nll = zero_inflated_poisson(nat, lmu, f32(lpi)).eval()
    plot(nll, axe)
    axe.set_title(f"lpi = {lpi}")
plt.show()

fig, axes = plt.subplots(ncols= 3, nrows= 3)
for axs, lth in zip(axes,    (-200, 0, 200)):
    for axe, lpi in zip(axs, (-200, 0, 200)):
        nll = zero_inflated_negative_binomial(nat, lmu, f32(lth), f32(lpi)).eval()
        plot(nll, axe)
        axe.set_title(f"lth = {lth}, lpi = {lpi}")
plt.show()

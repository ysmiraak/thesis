from src.count.param import config as C, paths as P, train as T
from src.util.fn import partial, comp, select
from src.util.io import pform, load_txt, save_txt
from src.util.np import np, vpack, sample, batch_sample
from src.util.sp import load_spm, encode, decode
from src.util.tf import tf, pipe, batch_run
from tqdm import tqdm
tf.set_random_seed(C.seed)

import sys
lossfn = sys.argv[1]
if   lossfn == "p":
    C.trial += "p_"
    from src.count.model_p import Model
elif lossfn == "nb":
    C.trial += "nb_"
    from src.count.model_nb import Model
elif lossfn == "zip":
    C.trial += "zip_"
    from src.count.model_zip import Model
elif lossfn == "zinb":
    C.trial += "zinb_"
    from src.count.model_zinb import Model
else:
    raise ValueError(f"unknown lossfn {lossfn}")




C.ckpt = 4

# load data
vocab_src = load_spm(pform(P.data, "vocab_src"))
vocab_tgt = load_spm(pform(P.data, "vocab_tgt"))
src_valid =  np.load(pform(P.data, "valid_src.npy"))
tgt_valid =  np.load(pform(P.data, "valid_tgt.npy"))

# build model
m = Model.new(**select(C, *Model._new)).data().valid()
sess = tf.InteractiveSession()
saver = tf.train.Saver()
saver.restore(sess, pform(P.ckpt, C.trial, C.ckpt))


bowt, lmu, lpi = map(np.concatenate, zip(
    *batch_run(128, (m.bowt, m.lmu, m.lpi), {m.src_: src_valid, m.tgt_: tgt_valid})))
mu, pi = np.exp(lmu), 1/(1+np.exp(-lpi))

errs = lambda t, p: np.abs(t - p).sum(1).mean()




bowp = np.round(np.minimum(4, (1 - pi) * mu)) # 6.413330078125
bowp = np.round(np.minimum(4, np.where(pi < 0.5, mu, 0))) # 6.502197265625
bowp = np.round(np.minimum(4, np.where(pi < 0.5, (1 - pi) * mu, 0))) # 6.322021484375



errs(bowt, bowp)




bowp = np.minimum(bowp, 4)
e = np.abs(bowt - bowp).sum(1)
plt.hist(e, bins= 50)
plt.show()

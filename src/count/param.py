from src.param import Record, config, paths, train


config = Record(
    config
    , trial = "count_"
    , ckpt  = None
    , seed  = 0
)

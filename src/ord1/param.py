from src.param import Record, config, paths, train


config = Record(
    config
    , trial = "ord1_"
    , ckpt  = None
    , seed  = 0
)

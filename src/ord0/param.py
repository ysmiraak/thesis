from src.param import Record, config, paths, train


config = Record(
    config
    , trial = "ord0_"
    , ckpt  = None
    , seed  = 0
)

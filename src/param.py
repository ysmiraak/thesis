from src.util.fn import Record


config = Record(
    trial  = 'master_'
    , ckpt = None
    , seed = 0
    ### data
    , eos = 0
    , bos = 1
    , unk = 2
    , cap = 32
    ### model
    , dim_src = 8192
    , dim_tgt = 8192
    , dim_emb = 512
    , dim_mid = 2048
    ### batch
    , batch_train = 256
    , batch_infer = 512
    , batch_valid = 1024
    , total_valid = 4096
)


paths = Record(
      log = "~/cache/tensorboard-logdir"
    , raw = "lab/raw"
    , data = "lab/data"
    , ckpt = "lab/ckpt"
    , pred = "lab/pred"
)


train = Record(
      dropout = 0.1
    , smooth  = 0.1
    , warmup  = 4e3
    , beta1   = 0.9
    , beta2   = 0.98
    , epsilon = 1e-9
)

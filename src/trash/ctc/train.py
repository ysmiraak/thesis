from src.ctc.model import Model, batch_run
from src.ctc.param import config as C, paths as P, train as T
from src.util.fn import partial, comp, select
from src.util.io import pform, load_txt, save_txt
from src.util.np import np, vpack, sample, batch_sample
from src.util.sp import load_spm, encode
from src.util.tf import tf, pipe
from tqdm import tqdm
tf.set_random_seed(C.seed)

def decode(vocab, array):
    """-> str

    decodes `array : array int` with `vocab : SentencePieceProcessor`.
    if `array` has a higher rank, generates the results instead.

    """
    if 1 < array.ndim: return (decode(vocab, arr) for arr in array)
    return vocab.decode_ids(list(map(int, array)))

#############
# load data #
#############

vocab_src = load_spm(pform(P.data, "vocab_src"))
vocab_tgt = load_spm(pform(P.data, "vocab_tgt"))
src_valid =  np.load(pform(P.data, "valid_src.npy"))
tgt_valid =  np.load(pform(P.data, "valid_tgt.npy"))

def batch( size= C.batch_train
          , src= np.load(pform(P.data, "train_src.npy"))
          , tgt= np.load(pform(P.data, "train_tgt.npy"))):
    for i in batch_sample(len(src), size):
        yield src[i], tgt[i]

###############
# build model #
###############

model = Model.new(**select(C, *Model._new))
modat = model.data()
valid = modat.valid()
train = model.data(*pipe(batch, (tf.int32, tf.int32), prefetch= 16)).train(**T)

############
# training #
############

sess = tf.InteractiveSession()
saver = tf.train.Saver()
if C.ckpt:
    saver.restore(sess, pform(P.ckpt, C.trial, C.ckpt))
else:
    tf.global_variables_initializer().run()
# tf.summary.FileWriter(pform(P.log, C.trial), sess.graph).close()

def log(step
        , wtr= tf.summary.FileWriter(pform(P.log, C.trial))
        , log= tf.summary.scalar('step_loss', valid.loss)):
    loss = np.mean(np.concatenate(list(batch_run(
        sess= sess
        , model= valid
        , fetch= valid.loss_samp
        , src= src_valid
        , tgt= tgt_valid
        , batch= C.batch_valid))))
    wtr.add_summary(sess.run(log, {valid.loss: loss}), step)
    wtr.flush()

def translate(src, model= valid):
    for preds in batch_run(sess, model, model.pred, src, batch= C.batch_valid):
        yield from decode(vocab_tgt, preds)

for _ in range(6):
    for _ in tqdm(range(100), ncols= 70): # ~6.785 epochs
        any(sess.run(train.up) for _ in range(100))
        log(sess.run(train.step))
    step = sess.run(train.step)
    saver.save(sess, pform(P.ckpt, C.trial, step // 10000), write_meta_graph= False)
    save_txt(        pform(P.pred, C.trial, step // 10000), translate(src_valid))

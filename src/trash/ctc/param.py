from src.param import Record, config, paths, train


config = Record(
    config
    , trial = "ctc_"
    , ckpt  = None
    , seed  = 0
    , batch_train = 128
    , batch_infer = 128
    , batch_valid = 128
)

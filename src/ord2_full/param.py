from src.param import Record, config, paths, train


config = Record(
    config
    , trial = "ord2_full_"
    , ckpt  = None
    , seed  = 0
)

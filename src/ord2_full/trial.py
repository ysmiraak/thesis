from src.ord2_full.model import Model
from src.ord2_full.param import config as C, paths as P, train as T
from src.util.fn import partial, comp, select
from src.util.io import pform, load_txt, save_txt
from src.util.np import np, vpack, sample, batch_sample
from src.util.sp import load_spm, encode, decode
from src.util.tf import tf, pipe, batch_run
from tqdm import tqdm
tf.set_random_seed(C.seed)

C.ckpt = 4

# load data
vocab_src = load_spm(pform(P.data, "vocab_src"))
vocab_tgt = load_spm(pform(P.data, "vocab_tgt"))
src_valid =  np.load(pform(P.data, "valid_src.npy"))
tgt_valid =  np.load(pform(P.data, "valid_tgt.npy"))

# build model
m = Model.new(**select(C, *Model._new)).data().valid()
sess = tf.InteractiveSession()
saver = tf.train.Saver()
saver.restore(sess, pform(P.ckpt, C.trial, C.ckpt))




src = src_valid[:4]
tgt = tgt_valid[:4]
bowt = sess.run(m.bowt, {m.tgt_: tgt})
bowp = sess.run(m.bowp, {m.src_: src})
tgtt = sess.run(m.tgt, {m.bowp: bowt})
tgtp = sess.run(m.tgt, {m.bowp: bowp})

bowt = np.concatenate(list(batch_run(128, m.bowt, {m.tgt_: tgt_valid})))
bowp = np.concatenate(list(batch_run(128, m.bowp, {m.src_: src_valid})))

from src.ord2_full.model import Model
from src.ord2_full.param import config as C, paths as P, train as T
from src.util.fn import partial, comp, select
from src.util.io import pform, load_txt, save_txt
from src.util.np import np, vpack, sample, batch_sample
from src.util.sp import load_spm, encode, decode
from src.util.tf import tf, pipe, batch_run
from tqdm import tqdm
tf.set_random_seed(C.seed)

#############
# load data #
#############

vocab_src = load_spm(pform(P.data, "vocab_src"))
vocab_tgt = load_spm(pform(P.data, "vocab_tgt"))
src_valid =  np.load(pform(P.data, "valid_src.npy"))
tgt_valid =  np.load(pform(P.data, "valid_tgt.npy"))

def batch( size= C.batch_train
          , src= np.load(pform(P.data, "train_src.npy"))
          , tgt= np.load(pform(P.data, "train_tgt.npy"))):
    for i in batch_sample(len(src), size):
        yield src[i], tgt[i]

###############
# build model #
###############

model = Model.new(**select(C, *Model._new))
valid = model.data().valid()
train = model.data(*pipe(batch, (tf.int32, tf.int32))).train(**T)

############
# training #
############

sess = tf.InteractiveSession()
saver = tf.train.Saver()
if C.ckpt:
    saver.restore(sess, pform(P.ckpt, C.trial, C.ckpt))
else:
    tf.global_variables_initializer().run()

def log(step
        , wtr= tf.summary.FileWriter(pform(P.log, C.trial))
        , log= tf.summary.merge(
            ( tf.summary.scalar('step_bowl', valid.bowl)
            , tf.summary.scalar('step_errt', valid.errt)
            , tf.summary.scalar('step_loss', valid.loss)))
        , inp= (valid.bowl     , valid.errt     , valid.loss     )
        , fet= (valid.bowl_samp, valid.errt_samp, valid.loss_samp)
        , fed= {valid.src_: src_valid, valid.tgt_: tgt_valid}
        , bat= C.batch_valid):
    wtr.add_summary(log.eval(dict(zip(
        inp, map(comp(np.mean, np.concatenate), zip(
            *batch_run(bat, fet, fed)))))), step)
    wtr.flush()

def translate(m= valid, src= src_valid, bat= C.batch_valid):
    for lens, pred in batch_run(bat, (m.len_tgt, m.pred), {m.src_: src}):
        i = 0
        for j in np.cumsum(lens):
            yield decode(vocab_tgt, pred[i:j])
            i = j

for _ in range(6):
    for _ in tqdm(range(100), ncols= 70): # ~13.57 epochs
        any(sess.run(train.down) for _ in range(100))
        log(sess.run(train.step))
    step  = sess.run(train.step)
    saver.save(sess, pform(P.ckpt, C.trial, step // 10000), write_meta_graph= False)
    save_txt(        pform(P.pred, C.trial, step // 10000), translate())

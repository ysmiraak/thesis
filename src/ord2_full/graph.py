from src.ord2_full.model import Model
from src.ord2_full.param import config as C, paths as P, train as T
from src.util.fn import select
from src.util.io import pform
from src.util.tf import tf, scope

with scope('model'): model = Model.new(**select(C, *Model._new))
with scope('mode0'): mode0 = model.data().valid(connect= 0)
with scope('mode1'): mode1 = model.data().valid(connect= 1)
with scope('mode2'): mode2 = model.data().valid(connect= 2)
# with scope('mode0'): mode0 = model.data().train(**T, connect= 0)
# with scope('mode1'): mode1 = model.data().train(**T, connect= 1)
# with scope('mode2'): mode2 = model.data().train(**T, connect= 2)

tf.summary.FileWriter(pform(P.log, C.trial), tf.get_default_graph()).close()

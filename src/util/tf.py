from src.util.fn import partial
from src.util.np import partition
import tensorflow as tf


f32 = partial(tf.cast, dtype= tf.float32, name= 'f32')
i32 = partial(tf.cast, dtype= tf.int32  , name= 'i32')
scope = tf.variable_scope


def profile(wtr, run, feed_dict= None, prerun= 3, tag= 'flow', sess= None):
    if sess is None: sess = tf.get_default_session()
    for _ in range(prerun): sess.run(run, feed_dict)
    meta = tf.RunMetadata()
    sess.run(run, feed_dict, tf.RunOptions(trace_level= tf.RunOptions.FULL_TRACE), meta)
    wtr.add_run_metadata(meta, tag)


def batch_run(size, node, feed, misc= None, sess= None):
    if sess is None: sess = tf.get_default_session()
    nrow = set(map(len, feed.values()))
    assert 1 == len(nrow)
    nrow = nrow.pop()
    misc = dict(misc or {})
    for i, j in partition(nrow, size, discard= False):
        misc.update(((t, x[i:j]) for t, x in feed.items()))
        yield sess.run(node, misc)


def pipe(*args, prefetch= 1, repeat= -1, name= 'pipe', **kwargs):
    """see `tf.data.Dataset.from_generator`"""
    with scope(name):
        return tf.data.Dataset.from_generator(*args, **kwargs) \
                              .repeat(repeat) \
                              .prefetch(prefetch) \
                              .make_one_shot_iterator() \
                              .get_next()


def placeholder(dtype, shape, x= None, name= None):
    """returns a placeholder with `dtype` and `shape`

    if tensor `x` is given, converts and uses it as default

    """
    if x is None: return tf.placeholder(dtype, shape, name)
    try:
        x = tf.convert_to_tensor(x, dtype)
    except ValueError:
        x = tf.cast(x, dtype)
    return tf.placeholder_with_default(x, shape, name)


def size(x, axis= None, name= 'size'):
    """returns the shape of `x` as a tuple of integers (static) or int32
    scalar tensors (dynamic).

    if `axis` is not None, returns only that dimension instead.

    """
    with scope(name):
        shape = tf.shape(x)
        shape = tuple(d if d is not None else shape[i] for i, d in enumerate(x.shape.as_list()))
        return shape if axis is None else shape[axis]


def variable(name, shape, init= 'rand', initializers=
             {  'zero': tf.initializers.zeros()
              , 'unit': tf.initializers.ones()
              , 'rand': tf.glorot_uniform_initializer()
             }):
    """wraps `tf.get_variable` to provide initializer based on usage"""
    return tf.get_variable(name, shape, initializer= initializers.get(init, init))


def trim(x, eos= 0, cap= 2147483647, pad= 0, name= 'trim'):
    """trims a tensor of sequences

    x   : tensor i32 (b, ?)
    eos : tensor i32 ()
       -> tensor i32 (b, t)  the trimmed sequence tensor
        , tensor b8  (b, t)  the sequence mask
        , tensor i32 (b,)    the non-eos sequence lengths
        , tensor i32 ()      the maximum non-eos sequence length t

    each row aka sequence in `x` is assumed to be any number of
    non-eos followed by any number of eos.

    if some non-eos length excedes `cap`, the sequence is truncated to
    ensure that `t <= cap`.

    if `0 != pad`, all sequences are guaranteed to have at least that
    many eos at the end.  this is done without breaking the `cap`, and
    the number is counted into the sequence lengths.

    """
    cap -= pad
    with scope(name):
        with scope('not_eos'): not_eos = tf.not_equal(x, eos)
        with scope('len_seq'): len_seq = tf.minimum(cap, tf.reduce_sum(i32(not_eos), axis= 1))
        with scope('max_len'): max_len = tf.reduce_max(len_seq)
        with scope('trim'): x, not_eos = x[:,:max_len], not_eos[:,:max_len]
        if 0 != pad: # this allows pad to be a tensor
            with scope('pad'):
                x       = tf.pad(x      , ((0,0),(0,pad)), constant_values= eos)
                not_eos = tf.pad(not_eos, ((0,0),(pad,0)), constant_values= True)
                len_seq += pad
                max_len += pad
        return x, not_eos, len_seq, max_len


def repeat(x, n, axis= 0):
    """same as `np.repeat`"""
    return tf.gather(x, tf.ragged.range(n).value_rowids(), axis= axis)


def bow2seq(bow, eos, reverse= False, name= 'bow2seq'):
    """tensor i32 (b, n) -> tensor i32[n] (b, ?)

    converts a batch of bag-of-word vectors to sequences of ascending
    values, padded with `eos`.

    """
    with scope(name):
        gtz = tf.greater(bow, 0)
        row, val = tf.unstack(repeat(i32(tf.where(gtz)), tf.boolean_mask(bow, gtz)), axis= 1)
        rag = tf.RaggedTensor.from_value_rowids(val, row, nrows= size(bow, 0))
        seq = rag.to_tensor(eos)
        if reverse: seq = tf.reverse_sequence(seq, rag.row_lengths(), seq_axis= 1)
        return seq


def seq2bow(seq, dim, mask= None, name= 'seq2bow'):
    """tensor i32 (b, ?) -> tensor i32 (b, dim)

    converts a batch of sequences to bag-of-word vectors.

    """
    with scope(name):
        bat = size(seq, 0)
        bxd = bat * dim
        seq += tf.expand_dims(dim * tf.range(bat), 1)
        if mask is not None: seq = tf.boolean_mask(seq, mask)
        bow = tf.bincount(seq, minlength= bxd, maxlength= bxd)
        return tf.reshape(bow, (bat, dim))


# tgt = tf.constant(
#     [[8, 4, 2, 4, 6, 4, 0, 0],
#      [4, 4, 4, 4, 0, 0, 0, 0],
#      [0, 0, 0, 0, 0, 0, 0, 0],
#      [4, 3, 2, 1, 0, 0, 0, 0],
#      [1, 2, 3, 4, 0, 0, 0, 0]])
# bow = seq2bow(tgt, dim= 10, mask= tf.not_equal(tgt, 0))
# seq = bow2seq(bow, eos= 0, reverse= True)


def poisson(x, mu, lmu= None, eps= 1e-7, name= 'poisson'):
    with scope(name):
        if lmu is None: lmu = tf.log(eps + mu)
        return mu - (lmu * x) + tf.lgamma(x + 1.0)


def zero_inflated_poisson(x, mu, pi, lmu= None, eps= 1e-7, name= 'zero_inflated_poisson'):
    with scope(name):
        if lmu is None: lmu = tf.log(eps + mu)
        return tf.where(
            0.0 < x
            , poisson(x, mu, lmu) - tf.log1p(eps - pi)
            , - tf.log(eps + pi + (1.0 - pi) * tf.exp(- mu)))


def negative_binomial(x, mu, th, lmu= None, rth= None, eps= 1e-7, name= 'neg_binom'):
    # mode = relu(mu * (1 - (1 / th)))
    with scope(name):
        if lmu is None: lmu = tf.log(eps + mu)
        if rth is None: rth = tf.reciprocal(eps + th)
        return (rth + x) * tf.log(rth + mu) \
            -   rth * tf.log(rth) \
            -   x  * lmu \
            -   tf.lgamma(rth + x) \
            +   tf.lgamma(1.0 + x) \
            +   tf.lgamma(rth)


def zero_inflated_negative_binomial(x, mu, th, pi, lmu= None, rth= None, eps= 1e-7, name= 'zero_inflated_negative_binomial'):
    with scope(name):
        if lmu is None: lmu = tf.log(eps + mu)
        if rth is None: rth = tf.reciprocal(eps + th)
        return tf.where(
            0.0 < x
            , negative_binomial(x, mu, th, lmu= lmu, rth= rth) - tf.log1p(eps - pi)
            , - tf.log(eps + pi + ((1.0 - pi) * ((rth / (rth + mu)) ** rth))))

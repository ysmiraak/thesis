from src.util.fn import partial, Record
from src.util.tf import tf, scope, placeholder, size, variable


class Normalize(Record):
    """layer normalization"""

    def __init__(self, dim, name= 'normalize'):
        self.name = name
        with scope(name):
            self.gain = variable('gain', dim, 'unit')
            self.bias = variable('bias', dim, 'zero')

    def __call__(self, x, axis= 1, name= None):
        rank = len(x.shape)
        assert -rank <= axis < rank
        axis = axis % rank
        shape = [(-1 if axis == i else 1) for i in range(rank)]
        with scope(name or self.name):
            mean, var = tf.nn.moments(x, axis, keep_dims= True)
            return (x - mean) * tf.rsqrt(var + 1e-12) \
                * tf.reshape(self.gain, shape) \
                + tf.reshape(self.bias, shape)


class Smooth(Record):
    """binary smoothing if dim is None or channel-last one-hot smoothing"""

    def __init__(self, rate, dim= None, name= 'smooth'):
        self.dim = dim
        self.name = name
        with scope(name):
            self.rate = placeholder(tf.float32, (), rate, 'rate')
            self.shared = self.rate / (dim or 2)
            self.smooth = 1.0 - self.rate

    def __call__(self, x, name= None):
        with scope(name or self.name):
            if self.dim:
                return tf.one_hot(x, self.dim, self.smooth + self.shared, self.shared)
            else:
                return x * self.smooth + self.shared


class Dropout(Record):
    """dropout shape may contain None (to be dynamically filled) or 1 (to
    be broadcasted) or some fixed dimension, such as `(None, 256, 1)`

    """

    def __init__(self, rate, shape= None, name= 'dropout'):
        self.shape = shape
        self.name = name
        with scope(name):
            self.rate = placeholder(tf.float32, (), rate, 'rate')

    def __call__(self, x, name= None):
        with scope(name or self.name):
            shape = size(x)
            if self.shape is not None:
                shape = [d or shape[i] for i, d in enumerate(self.shape)]
            return tf.nn.dropout(x, rate= self.rate, noise_shape= shape)


class Embed(Record):
    """input and output embedding

    tensor i32 (b, t) -> tensor f32 (b, n, t)
    tensor f32 (?, n) -> tensor f32 (?, m)

    """

    def __init__(self, n, m, name= 'embed'):
        self.name = name
        with scope(name):
            self.logit = variable('kern', (n, m))
            self.embed = tf.transpose(self.logit) * (n ** 0.5)

    def __call__(self, x, name= None):
        with scope(name or self.name):
            if x.dtype.is_integer:
                assert 2 == len(x.shape)
                return tf.transpose(tf.gather(self.embed, x), (0, 2, 1))
            else:
                return x @ self.logit


class Conv(Record):
    """convolution from `m` to `n` channels

    the default parameters make a position-wise linear layer

    """

    def __init__(self, n, m= None, size= 1, name= 'conv'):
        if m is None: m = n
        self.name = name
        with scope(name):
            self.kern = variable('kern', (size, m, n))

    def __call__(self, x, name= None):
        with scope(name or self.name):
            return tf.nn.convolution(x, self.kern, padding= 'VALID', data_format= 'NCW')

    def shape(self):
        return size(self.kern)


class Attention(Record):
    """computes multi-head scaled dot-product attention

    query : tensor f32 (b, d_q, t)
    value : tensor f32 (b, d_v, s)
     mask : tensor f32 (b,   t, s)
         -> tensor f32 (b, d_q, t)

    `dim` must be divisible by `head`

    `mask` has on-values 0 and off-values -inf

    """

    def __init__(self, dim, d_q= None, d_v= None, head= 8, name= 'attention'):
        assert not dim % head
        if d_q is None: d_q = dim
        if d_v is None: d_v = dim
        self.dim  = dim
        self.head = head
        self.name = name
        with scope(name):
            self.v = Conv(dim, d_v, name= 'v')
            self.k = Conv(dim, d_v, name= 'k')
            self.q = Conv(dim, d_q, name= 'q')
            self.p = Conv(d_q, dim, name= 'p')

    def __call__(self, query, value, mask= None, name= None):
        with scope(name or self.name):
            d,h,c = self.dim, self.head, self.dim // self.head
            _,_,t = size(query)
            _,_,s = size(value)
            # pretransformations
            v = tf.reshape(self.v(value), (-1,h,c,s)) # bhcs <- bds <- bvs
            k = tf.reshape(self.k(value), (-1,h,c,s)) # bhcs <- bds <- bvs
            q = tf.reshape(self.q(query), (-1,h,c,t)) # bhct <- bdt <- bqt
            # weight
            a = tf.matmul(q, k, transpose_a= True) # bhts <- (bhtc <- bhct) @ bhcs
            a *= c ** -0.5
            if mask is not None: a += tf.expand_dims(mask, axis= 1)
            a = tf.nn.softmax(a, axis= -1)
            # attend
            y = tf.matmul(v, a, transpose_b= True) # bhct <- bhcs @ (bhst <- bhts)
            # posttransformation
            return self.p(tf.reshape(y, (-1,d,t)))

from src.util.fn import Record, identity
from src.util.nn import Normalize, Smooth, Dropout, Embed, Conv, Attention
from src.util.np import np, partition
from src.util.tf import tf, f32, scope, placeholder, trim


def causal_mask(t, name= 'causal_mask'):
    """returns the causal mask for `t` steps"""
    with scope(name):
        return tf.linalg.LinearOperatorLowerTriangular(tf.ones((t, t))).to_dense()


def sinusoid(dim, time, freq= 1e-4, array= False):
    """returns a rank-2 tensor of shape `dim, time`, where each column
    corresponds to a time step and each row a sinusoid, with
    frequencies in a geometric progression from 1 to `freq`.

    """
    assert not dim % 2
    if array:
        a = (freq ** ((2 / dim) * np.arange(dim // 2))).reshape(-1, 1) @ (1 + np.arange(time).reshape(1, -1))
        return np.concatenate((np.sin(a), np.cos(a)), -1).reshape(dim, time)
    else:
        assert False # figure out a better way to do this
        a = tf.reshape(
            freq ** ((2 / dim) * tf.range(dim // 2, dtype= tf.float32))
            , (-1, 1)) @ tf.reshape(
                1 + tf.range(f32(time), dtype= tf.float32)
                , (1, -1))
        return tf.reshape(tf.concat((tf.sin(a), tf.cos(a)), axis= -1), (dim, time))


class Sinusoid(Record):

    def __init__(self, dim, cap= None, name= 'sinusoid'):
        self.dim = dim
        self.name = name
        with scope(name):
            self.pos = tf.constant(sinusoid(dim, cap, array= True), tf.float32) if cap else None

    def __call__(self, time, name= None):
        with scope(name or self.name):
            return sinusoid(self.dim, time) if self.pos is None else self.pos[:,:time]


class MlpBlock(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.lin  = Conv(4*dim, dim, name= 'lin')
            self.lex  = Conv(dim, 4*dim, name= 'lex')
            self.norm = Normalize(dim)

    def __call__(self, x, dropout, name= None):
        with scope(name or self.name):
            return self.norm(x + dropout(self.lex(tf.nn.relu(self.lin(x)))))


class AttBlock(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.att  = Attention(dim)
            self.norm = Normalize(dim)

    def __call__(self, x, v, m, dropout, name= None):
        with scope(name or self.name):
            return self.norm(x + dropout(self.att(x, v, m)))


class Encode(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.blocks = AttBlock(dim, 's1') \
                ,         MlpBlock(dim, 'm1') \
                ,         AttBlock(dim, 's2') \
                ,         MlpBlock(dim, 'm2') \
                ,         AttBlock(dim, 's3') \
                ,         MlpBlock(dim, 'm3') \
                # ,         AttBlock(dim, 's4') \
                # ,         MlpBlock(dim, 'm4') \
                # ,         AttBlock(dim, 's5') \
                # ,         MlpBlock(dim, 'm5') \
                # ,         AttBlock(dim, 's6') \
                # ,         MlpBlock(dim, 'm6')

    def __call__(self, x, m, dropout, name= None):
        with scope(name or self.name):
            for block in self.blocks:
                btype = block.name[0]
                if   'c' == btype: x = block(x, dropout)
                elif 's' == btype: x = block(x, x, m, dropout)
                elif 'm' == btype: x = block(x, dropout)
                else: raise TypeError('unknown encode block')
            return x


class Decode(Record):

    def __init__(self, dim, name):
        self.name = name
        with scope(name):
            self.blocks = AttBlock(dim, 's1') \
                ,         AttBlock(dim, 'a1') \
                ,         MlpBlock(dim, 'm1') \
                ,         AttBlock(dim, 's2') \
                ,         AttBlock(dim, 'a2') \
                ,         MlpBlock(dim, 'm2') \
                ,         AttBlock(dim, 's3') \
                ,         AttBlock(dim, 'a3') \
                ,         MlpBlock(dim, 'm3') \
                # ,         AttBlock(dim, 's4') \
                # ,         AttBlock(dim, 'a4') \
                # ,         MlpBlock(dim, 'm4') \
                # ,         AttBlock(dim, 's5') \
                # ,         AttBlock(dim, 'a5') \
                # ,         MlpBlock(dim, 'm5') \
                # ,         AttBlock(dim, 's6') \
                # ,         AttBlock(dim, 'a6') \
                # ,         MlpBlock(dim, 'm6')

    def __call__(self, x, m, w, n, dropout, name= None):
        with scope(name or self.name):
            for block in self.blocks:
                btype = block.name[0]
                if   'c' == btype: x = block(x, dropout)
                elif 'b' == btype: x = block(x, x, m, w, n, dropout)
                elif 's' == btype: x = block(x, x, m, dropout)
                elif 'a' == btype: x = block(x, w, n, dropout)
                elif 'm' == btype: x = block(x, dropout)
                else: raise TypeError('unknown decode block')
            return x


class Model(Record):
    """-> Record

    model = Model.new( ... )
    train = model.data( ... ).train( ... )
    valid = model.data( ... ).valid( ... )

    """
    _new = 'dim_emb', 'dim_mid', 'dim_src', 'dim_tgt', 'cap', 'eos', 'bos'

    @staticmethod
    def new(dim_emb, dim_mid, dim_src, dim_tgt, cap, eos, bos):
        """-> Model with fields

          decode : Decode
          encode : Encode
         emb_tgt : Embed
         emb_src : Embed

        """
        assert not dim_emb % 2
        return Model(
              decode= Decode(dim_emb, name= 'decode')
            , encode= Encode(dim_emb, name= 'encode')
            , emb_tgt= Embed(dim_emb, dim_tgt, name= 'emb_tgt')
            , emb_src= Embed(dim_emb, dim_src, name= 'emb_src')
            , dim_emb= dim_emb
            , dim_tgt= dim_tgt
            , bos= bos
            , eos= eos
            , cap= cap + 1)

    def data(self, src= None, tgt= None):
        """-> Model with new fields

        position : Sinusoid
            src_ : i32 (b, ?)     source feed, in range `[0, dim_src)`
            tgt_ : i32 (b, ?)     target feed, in range `[0, dim_tgt)`
             src : i32 (b, s)     source with `eos` trimmed among the batch
             tgt : i32 (b, t)     target with `eos` trimmed among the batch
            mask : b8  (b, t)     target sequence mask
            true : i32 (?,)       target references
         max_tgt : i32 ()         maximum target length
         max_src : i32 ()         maximum source length
        mask_tgt : f32 (1, t, t)  target attention mask
        mask_src : f32 (b, 1, s)  source attention mask

        """
        src_ = placeholder(tf.int32, (None, None), src, 'src_')
        tgt_ = placeholder(tf.int32, (None, None), tgt, 'tgt_')
        with scope('src'):
            src, mask, len_src, max_src = trim(src_, eos= self.eos, cap= self.cap)
            mask_src = tf.log(tf.expand_dims(f32(mask), axis= 1))
        with scope('tgt'):
            tgt, mask, len_tgt, max_tgt = trim(tgt_, eos= self.eos, cap= self.cap)
            mask_tgt = tf.log(tf.expand_dims(f32(mask), axis= 1))
            true,tgt = tgt, tf.sort(true, axis= -1, direction= 'DESCENDING')
        return Model(
            position= Sinusoid(self.dim_emb, self.cap)
            , src_= src_, mask_src= mask_src, len_src= len_src, max_src= max_src, src= src
            , tgt_= tgt_, mask_tgt= mask_tgt, len_tgt= len_tgt, max_tgt= max_tgt, tgt= tgt
            , true= true, mask= mask
            , **self)

    def valid(self, dropout= identity, smooth= None):
        """-> Model with new fields, teacher forcing

           output : f32 (?, dim_tgt)  prediction on logit scale
             prob : f32 (?, dim_tgt)  prediction, soft
             pred : i32 (?,)          prediction, hard
        errt_samp : f32 (?,)          errors
        loss_samp : f32 (?,)          losses
             errt : f32 ()            error rate
             loss : f32 ()            mean loss

        """
        with scope('emb_src_'): w = self.position(self.max_src) + dropout(self.emb_src(self.src))
        with scope('emb_tgt_'): x = self.position(self.max_tgt) + dropout(self.emb_tgt(self.tgt))
        w = self.encode(w, self.mask_src,                   dropout, name= 'encode_') # bds
        x = self.decode(x, self.mask_tgt, w, self.mask_src, dropout, name= 'decode_') # bdt
        with scope('logit_'):
            y = self.emb_tgt( # ?n
                tf.boolean_mask( # ?d
                    tf.transpose(x, (0,2,1)) # btd <- bdt
                    , self.mask))
        with scope('prob_'): prob = tf.nn.softmax(y, axis= -1)
        with scope('pred_'): pred = tf.argmax(y, axis= -1, output_type= tf.int32)
        with scope('true_'): true = tf.boolean_mask(self.true, self.mask)
        with scope('errt_'):
            errt_samp = f32(tf.not_equal(true, pred))
            errt = tf.reduce_mean(errt_samp)
        with scope('loss_'):
            loss_samp = tf.nn.softmax_cross_entropy_with_logits_v2(labels= smooth(true), logits= y) \
                if smooth else tf.nn.sparse_softmax_cross_entropy_with_logits(labels= true, logits= y)
            loss = tf.reduce_mean(loss_samp)
        return Model(self, output= y, prob= prob, pred= pred
                     , errt_samp= errt_samp, errt= errt
                     , loss_samp= loss_samp, loss= loss)

    def train(self, dropout= 0.1, smooth= 0.1, warmup= 4e3, beta1= 0.9, beta2= 0.98, epsilon= 1e-9):
        """-> Model with new fields, teacher forcing

        step : i64 () global update step
          lr : f32 () learning rate for the current step
          up :        update operation

        along with all the fields from `valid`

        """
        dropout, smooth = Dropout(dropout, (None, self.dim_emb, None)), Smooth(smooth, self.dim_tgt)
        self = self.valid(dropout= dropout, smooth= smooth)
        with scope('lr'):
            s = tf.train.get_or_create_global_step()
            t = f32(s + 1)
            lr = (self.dim_emb ** -0.5) * tf.minimum(t ** -0.5, t * (warmup ** -1.5))
        up = tf.train.AdamOptimizer(lr, beta1, beta2, epsilon).minimize(self.loss, s)
        return Model(self, dropout= dropout, smooth= smooth, step= s, lr= lr, up= up)


def batch_run(sess, model, fetch, src, tgt= None, batch= None):
    if batch is None: batch = len(src)
    for i, j in partition(len(src), batch, discard= False):
        feed = {model.src_: src[i:j]}
        if tgt is not None:
            feed[model.tgt_] = tgt[i:j]
        yield sess.run(fetch, feed)

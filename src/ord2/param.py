from src.param import Record, config, paths, train


config = Record(
    config
    , trial = "ord2_"
    , ckpt  = None
    , seed  = 0
)

#!/usr/bin/env python3

from src.param import config as C, paths as P, train as T
from src.util.fn import partial
from src.util.io import pform, load_txt, save_txt
from src.util.np import np, vpack
from src.util.sp import spm

# http://www.manythings.org/anki/
path_txt = pform(P.raw, "deu.txt")
path_src = pform(P.raw, "src.txt")
path_tgt = pform(P.raw, "tgt.txt")

src_tgt = list(load_txt(path_txt))
np.random.seed(C.seed)
np.random.shuffle(src_tgt)
tgt,src = zip(*(line.split("\t") for line in src_tgt))
save_txt(path_src, src)
save_txt(path_tgt, tgt)
del src_tgt, src, tgt

###############
# build vocab #
###############

vocab_src = spm(pform(P.data, "vocab_src"), path_src, C.dim_src, C.eos, C.bos, C.unk)
vocab_tgt = spm(pform(P.data, "vocab_tgt"), path_tgt, C.dim_tgt, C.eos, C.bos, C.unk)

#############
# load data #
#############

src_tgt = list(zip(load_txt(path_src), load_txt(path_tgt)))
np.random.seed(C.seed)
np.random.shuffle(src_tgt)

####################
# filter and split #
####################

train_src = []
train_tgt = []
valid_src = []
valid_tgt = []
valid_raw = []
for src, tgt in src_tgt:
    s = vocab_src.encode_as_ids(src)
    t = vocab_tgt.encode_as_ids(tgt)
    if 0 < len(s) <= C.cap and 0 < len(t) <= C.cap:
        if len(valid_raw) < C.total_valid \
           and 6 <= len(s) and 6 <= len(t):
            valid_src.append(s)
            valid_tgt.append(t)
            valid_raw.append(tgt)
        else:
            train_src.append(s)
            train_tgt.append(t)

#############
# save data #
#############

vpack2 = partial(vpack, fill= C.eos, dtype= np.uint16)
np.save( pform(P.data, "train_src.npy"), vpack2(train_src, (len(train_src), max(map(len, train_src)))))
np.save( pform(P.data, "train_tgt.npy"), vpack2(train_tgt, (len(train_tgt), max(map(len, train_tgt)))))
np.save( pform(P.data, "valid_src.npy"), vpack2(valid_src, (len(valid_src), max(map(len, valid_src)))))
np.save( pform(P.data, "valid_tgt.npy"), vpack2(valid_tgt, (len(valid_tgt), max(map(len, valid_tgt)))))
save_txt(pform(P.data, "valid_tgt.txt"),        valid_raw)

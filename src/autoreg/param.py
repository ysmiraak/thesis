from src.param import Record, config, paths, train


config = Record(
    config
    , trial = "autoreg_"
    , ckpt  = None
    , seed  = 0
)

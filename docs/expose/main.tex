\documentclass[11pt]{article}
% \usepackage[margin=1.5in]{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\definecolor{darkblue}{rgb}{0,0,0.5}
\usepackage[colorlinks=true,allcolors=darkblue]{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{enumitem}
\setlist{noitemsep}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\graphicspath{{image/}}
\usepackage[sorting=ynt,style=authoryear,uniquename=false]{biblatex}
\addbibresource{main.bib}

\title{Master's thesis expos\'e: non-autoregressive models in neural machine translation}
\author{%
  Kuan Yu\\
  \texttt{kuanyu@uni-potsdam.de}\\
  \\
  Master's Program in \emph{Cognitive Systems}\\
  University of Potsdam}
\date{April 2019}

\begin{document}
\maketitle

\section{Introduction}

In natural language processing, we are often faced with tasks of making structure predictions.
The output of the model is not a single value but a collection of values arranged into a structured object,
such as a sequence (e.g.\ in tagging) or a tree/graph (e.g.\ in parsing).
Other generic examples include language generation, recognition, synthesis, and translation.

Traditionally, probabilistic graphical models,
either generative (e.g.\ hidden Markov models) or discriminative (e.g.\ conditional random fields),
played an important role.
Current approaches largely involve neural networks.
Recurrent neural network (RNN) with an encoder-decoder structure
has led to the success of many neural machine translation (NMT) systems
\parencite{kalchbrenner2013recurrent,sutskever2014sequence,cho2014learning,cho2014properties,wu2016google}.
Later works has adopted various attention mechanisms in conjunction with RNN
\parencite{bahdanau2014neural,luong2015effective}.
Convolutional networks (CNN) with attention have shown to be effective as well
\parencite{kalchbrenner2016neural,gehring2017convolutional,kaiser2017depthwise,bai2018empirical}.
More recent works favor a purely attention-based architecture named the Transformer
which has maintained the state-of-the-art
\parencite{vaswani2017attention,dehghani2018universal,ott2018scaling,so2019evolved}.
These models are predominantly autoregressive.

This project explores an alternative method for structured predictions,
namely non-autoregressive modeling,
in the task of neural machine translation.
We first explain autoregressive models and their inherent problems (Section~\ref{sec:autor-model}),
then we introduce popular methods for non-autoregressive modeling (Section~\ref{sec:non-autor-model}),
and finally we state the goal and the plan for this thesis project (Section~\ref{sec:proposal}).

\section{Autoregressive modeling}\label{sec:autor-model}

Commonly in an encoder-decoder network for NMT,
the encoder produces annotations over a sequence (usually a sentence) in the source language,
and the decoder autoregressively generates a sequence in the target language,
with each step conditioning on the annotations and previously generated partial sequence.
Figure~\ref{fig:autoreg} shows the corresponding graphical model.

\begin{figure}
  \centering\includegraphics[width=\linewidth]{autoreg.pdf}
  \caption[]{\label{fig:autoreg}An autoregressive encoder-decoder models
    the probability of a target sequence \(y\) given a source sequence \(x\),
    with the target sequence factorized causally by position:
    \small{\( p(y \mid x) = p(y_{1} \mid x)\ p(y_{2} \mid y_{1}, x)\ p(y_{3} \mid y_{2}, y_{1}, x)\ \ldots \)}}
\end{figure}

During inference, an output is sampled from the decoder predictions at each step,
and fed back together with the annotations as the input for the next step.
The sampling space for each step is the target vocabulary,
and the total search space grows exponentially.
Since the exact inference is intractable,
beam search is commonly used to approximate the optimal solution \parencite{freitag2017beam}.
A cheaper alternative is greedy decoding,
where the sampling is simply performed by picking the prediction with the highest probability.
For training an autoregressive model,
the teacher-forcing method is typically used \parencite{williams1989learning}.
Instead of running the feedback loop,
the true target sequence is given as input with a begin-of-sentence symbol padded at the front,
to predict the same sequence with an end-of-sentence symbol padded at the end.
Teacher-forcing makes the training for all steps more parallelizable.
Full parallelization can be achieved in CNNs and Transformers,
as well as RNNs with no hidden-to-hidden connections.

While teacher-forcing training is efficient and effective,
it introduces an exposure bias problem.
Due to the accumulation of errors during inference,
the target sequences encountered by the model can be very different
from the ground truth sequences given as reference.
This causes the predictions of the model to degenerate over the length of the sequence.
The common remedy is scheduled sampling,
where free-running examples are introduced during training
\parencite{daume2009search,bengio2015scheduled}.
This is however computationally expensive,
and theoretically ad hoc, since teacher-forcing training is derived from the maximum likelihood principle,
whereas free-running training has no satisfactory justification \parencite{williams1989learning}.
Reinforcement learning may be more appropriate than maximum likelihood optimization
\parencite{ranzato2015sequence,nguyen2017reinforcement},
which is however far more expensive.

In short, autoregressive models can be efficient to train but expensive to use.
Existing remedies for training and inference are ad hoc and expensive.

\section{Non-autoregressive modeling}\label{sec:non-autor-model}

Non-autoregressive models predict the whole target sequence at once instead of step-by-step.
Without factorizing the sequence by position, it avoids the aforementioned problems.
The search space can be fixed and does not grow exponentially with the length of the sequence.
Moreover, there is no accumulation of errors along lengthy predictions.
However, all existing non-autoregressive models underperform autoregressive ones.
Here we summarize some approaches which achieved relative successes.

\subsection*{Semi-autoregressive}

If the goal is simply to avoid overtly long sequences,
it can be achieved by generating a shorter hidden sequence,
and predict the full sequence from the hidden sequence deterministically and in parallel
\parencite{wang2018semi}.
This semi-autoregressive approach alleviates but does not eradicate the problem.

\subsection*{Denoising}

It has been observed that the partial sequences provided during autoregressive decoding can be defective.
\textcite{tang2017speeding} trained models for sentence representation learning
with teacher-forcing and random sampling,
and found that they achieve equivalent results.
They further experimented with non-autoregressive convolutional decoders,
and found them to be equivalent to autoregressive ones.
Therefore non-autoregressive decoding may be formulated as a denoising problem,
where the source sequence is treated as a noisy version of the target sequence.
\textcite{gu2017non} transformed the source sequence using utility features.
\textcite{lee2018deterministic} simply used the source sequence but performed iterative refinement.

\subsection*{Latent variables}

The main drawback of autoregressive models may also be the source of their strengths.
Probabilistic prediction requires ruling out less likely outcomes,
and in an autoregressive process, each step prunes the hypothesis space,
allowing the model to settle on one eventuality.
The pruning process may discard better predictions,
but it reduces uncertainties in a progressive manner.
A non-autoregressive model is faced with the same challenge.
Introducing latent variables allows the model to factorize the target distribution,
and variational inference on those latent variables facilitates pruning
\parencite{kingma1606improving,schmidt2018deep}.
Some latent variable models also adopt vector quantization
\parencite{roy2018theory,kaiser2018fast}.

\subsection*{Loss functions}

Parallel WaveNet was a particularly successful latent variable model
for speech synthesis \parencite{oord2017parallel}.
They argued that training a non-autoregressive model
directly with maximum likelihood using cross-entropy was impractical,
and developed the probability density distillation method.
\textcite{libovicky2018end} designed a simple model trained
with connectionist temporal classification \parencite{graves2006connectionist},
which suggests that the key to a competitive non-autoregressive model may not be the architectural design
but simply in finding the appropriate loss function for training.

% TODO maybe some algebra here

\section{Proposal}\label{sec:proposal}

The reason why non-autoregressive models underperform autoregressive ones is unknown.
Models with relative successes usually deviate from conventional ones in multiple aspects,
and it is often unclear which aspects are of crucial importance,
due to a lack of ablation report.
Moreover, many of these models cannot be trained end-to-end,
making them significantly more complicated than conventional models.
The difficulty in implementing a working non-autoregressive model further contributes
to the lack of studies in this topic.

For this project, we propose to start with an autoregressive Transformer,
and adapt it to a working non-autoregressive model using existing techniques.
Then we will conduct an ablation study by incrementally eliminating the differences,
and try to identify the essential components in non-autoregressive modeling.

It is not the goal of this project to invent a novel architecture or to refresh the state-of-the-art.
The goal is to provide an up-to-date survey of non-autoregressive modeling
and to better understand the nature of this topic,
in the hope of assisting the development of simple yet effective non-autoregressive models
and helping to make this topic a conventional field of studies.

\printbibliography[]
\end{document}

#!/usr/bin/env bash

# mkdir lab lab/raw lab/data lab/ckpt lab/pred

# python3 -m src.data

# python3 -m src.autoreg.train

python3 -m src.count.train p
python3 -m src.count.train nb
python3 -m src.count.train zip
python3 -m src.count.train zinb
